
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Main {

	public static List<String> files = new ArrayList<String>();
	public static String appName = "main";
	public static String mainName = "main";

	public static String genCom() {
		String result = "";

		result = "g++ -c " + mainName + ".cpp;";

		for(String s:files) {
			result = result + "g++ -c " + s + ".cpp;";
		}

		result += "g++ ";
		result += mainName;
		result += ".o ";

		for(String s:files) {
			result += s;
			result += ".o ";
		}
		result += "-o ";
		result += appName;
		result += " -lsfml-graphics -lsfml-window -lsfml-system -lGL -lGLU;";

		result += "./";
		result += appName;

		return result;
	}

	public static void main(String[] args) {
		boolean run = true;

		Scanner scan = new Scanner(System.in);

		String com = "";

		while(run) {
			System.out.print(">> ");
			String command = scan.nextLine();

			if(command.equals("quit")) {
				run = false;
				break;
			}

			if(command.equals("help")) {
				String help = "";

				help += "quit: quits\n";
				help += "help: displays this\n";
				help += "com: generates command\n";
				help += "add: adds class\n";
				help += "main: sets main class name\n";
				help += "app: sets app name\n";
				help += "del: deletes class\n";

				System.out.println(help);
			}

			if(command.equals("com")) {
				com = genCom();
				System.out.println(com);
			}

			if(command.equals("add")) {
				System.out.print("Name: ");
				String name = scan.nextLine();

				files.add(name);
			}

			if(command.equals("main")) {
				System.out.print("Main: ");
				String name = scan.nextLine();

				mainName = name;
			}

			if(command.equals("app")) {
				System.out.print("App: ");
				String name = scan.nextLine();

				appName = name;
			}

			if(command.equals("del")) {
				System.out.print("Name: ");
				String name = scan.nextLine();

				files.remove(name);
			}
		}
	}
}